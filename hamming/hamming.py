def distance(strand_a, strand_b):
    if len(strand_a) != len(strand_b):
        raise ValueError('Length unequal')
    count=0
    for i in range(len(strand_a)):
        if strand_a[i] != strand_b[i]:
            count = count+1
    return count
    # return len([x for i,x in enumerate(strand_a) if strand_a[i] != strand_b[i] ])
