def is_isogram(string):
    parsed = string.replace(' ','').replace('-','').lower()
    parsed_set = set(parsed)
    return len(parsed) == len(parsed_set)