song = [{"num":"first", "thing": "a Partridge in a Pear Tree"},
        {"num":"second", "thing": "two Turtle Doves"},
        {"num":"third", "thing":"three French Hens"},
        {"num":"fourth", "thing":"four Calling Birds"},
        {"num":"fifth", "thing":"five Gold Rings"},
        {"num":"sixth", "thing":"six Geese-a-Laying"},
        {"num":"seventh", "thing":"seven Swans-a-Swimming"},
        {"num":"eighth", "thing":"eight Maids-a-Milking"},
        {"num":"ninth", "thing":"nine Ladies Dancing"},
        {"num":"tenth", "thing":"ten Lords-a-Leaping"},
        {"num":"eleventh", "thing":"eleven Pipers Piping"},
        {"num":"twelfth", "thing":"twelve Drummers Drumming"}
        ]
def recite(start, end):
    if start != end:
        return [recite(n, n)[0] for n in range(start, end+1)]
    things = makestring(start)
    return [f'On the {song[end-1]["num"]} day of Christmas my true love gave to me: {things}.']


def makestring(num):
    if num == 1:
        return song[0]["thing"]
    string=''
    for i in reversed(range(num)):
        if i == 0:
            string = string + "and " + song[i]["thing"]
        else:
            string = string + song[i]["thing"] + ', '
    return string