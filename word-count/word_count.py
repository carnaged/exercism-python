import re
def word_count(phrase):
    count={}
    for word in  re.compile("[a-z]+\'?[a-z]|[0-9]").findall(phrase.lower()):
        count[word] = count.get(word,0) + 1 
    return {word: count[word] for word in count}
