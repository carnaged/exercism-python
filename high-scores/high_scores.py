class HighScores(object):
    def __init__(self, scores):
        self.scores = scores
    def scores(self):
        return self.scores
    def latest(self):
        return self.scores.pop()
    def personal_best(self):
        return sorted(self.scores).pop()
    def personal_top_three(self):
        return sorted(self.scores, reverse=True)[:3]


