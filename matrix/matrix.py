class Matrix(object):
    def __init__(self, matrix_string):
        self.chars=[]
        rows = matrix_string.split('\n')
        for row in rows:
            char_list = [int(char) for char in row.split(' ')]
            self.chars.append(char_list)

    def row(self, index):
        return self.chars[index-1]

    def column(self, index):
        return [row[index-1] for row in self.chars]